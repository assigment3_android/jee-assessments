package com.tonylin;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;





import org.apache.commons.lang3.StringUtils;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionSupport;


public class Lab2 extends ActionSupport{

	private String genre;
	private String title;
	private String author;
	private boolean fuzzySearch;
	
	private String selectedGenre;
	private List<String> genreList = new ArrayList<String>();
//	private String defaultSelectedGenre = "Classic";

	
	public Lab2(){
		Connection conn = DBConnection.createConnection();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.createStatement();
		
			rs= stmt.executeQuery("SELECT DISTINCT "+Book.GENRE+" FROM book");
			System.out.println("SELECT DISTINCT "+Book.GENRE+" FROM book    "+rs.getMetaData().getColumnCount());
			while(rs.next()){
				this.genreList.add(rs.getString(1));
			}
		
			System.out.println(this.genreList.size());
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(stmt!=null)
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			if(rs!=null)
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
	}
	public List<String> getGenreList() {
		return genreList;
	}


//	public String getDefaultSelectedGenre() {
//		return defaultSelectedGenre;
//	}


	public void setSelectedGenre(String selectedGenre) {
		this.selectedGenre = selectedGenre;
	}

	private List<Book> bookList = new ArrayList<Book>();
	
	public void setGenre(String genre) {
		this.genre = genre;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public void setAuthor(String author) {
		this.author = author;
	}


	public void setFuzzySearch(boolean fuzzySearch) {
		this.fuzzySearch = fuzzySearch;
	}


	public List<Book> getBookList() {
		return bookList;
	}


	public String doQuery(){
		
//		System.out.println("defaultSelectedGenre="+this.defaultSelectedGenre);
//		
//		if(this.getActionErrors().size()>0) {
//			return Action.SUCCESS;
//		}
		
		System.out.println("genre="+genre);
		System.out.println("title="+title);
		System.out.println("author="+author);
		System.out.println("isFuzzySearch="+fuzzySearch);
		System.out.println("selectedGenre="+this.selectedGenre);
		
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try{
			StringBuilder sb = new StringBuilder("SELECT * FROM book");
			
			
			
			
			Map<String, String> conditionsMap = new HashMap<String,String>();
			
			if(StringUtils.isNotBlank(this.author))
				conditionsMap.put("author", this.author);
			
			if (StringUtils.isNotBlank(this.genre))
				conditionsMap.put("genre",this.genre);
			
			if (StringUtils.isNotBlank(this.title))
				conditionsMap.put("title",this.title);
			
			if(StringUtils.isNotBlank(this.selectedGenre)){
				if(StringUtils.equalsIgnoreCase(this.selectedGenre, "-1")){
					conditionsMap.remove("genre");
				}else{
					conditionsMap.put("genre",this.selectedGenre);
				}
			}
			
			conn = DBConnection.createConnection();
			stmt = conn.createStatement();
			
			
			if(conditionsMap.size()>0){
				sb.append(" WHERE ");
				for (String key : conditionsMap.keySet()){
					if (this.fuzzySearch)
						sb.append(key +" LIKE '%"+ conditionsMap.get(key)+"%' AND ");
					else
						sb.append(key +"='"+ conditionsMap.get(key)+"' AND ");
				}
				
				String sqlStr = null;
				sqlStr = sb.substring(0, sb.length()-4);
				
				System.out.println(sqlStr);
				rs = stmt.executeQuery(sqlStr);
				
				
			}else{
				String sqlStr = sb.toString();
				System.out.println(sqlStr);
				rs = stmt.executeQuery(sqlStr);
				
			}
			
			
			this.bookList.clear();
			while(rs.next()){
				Book book = new Book();
				
				book.setBookId(rs.getInt(Book.BOOK_ID));
				book.setBlurb(rs.getString(Book.BLURB));
				book.setAuthor(rs.getString(Book.AUTHOR));
				book.setGenre(rs.getString(Book.GENRE));
				book.setIsbn(rs.getString(Book.ISBN));
				book.setTitle(rs.getString(Book.TITLE));
				
				this.bookList.add(book);
				
			}

			System.out.println(this.bookList.size());
			
			return Action.SUCCESS;
		}catch(SQLException sqle){
			sqle.printStackTrace();
			return Action.ERROR;
		}finally{
			if(rs!=null)
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			if(stmt!=null)
				try {
					stmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			/*
			if(conn!=null)
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			*/
		}
		
	}

	
	@Override
	public void validate() {
		System.out.println("validate.");
	//	System.out.println("actionError<"+this.getActionErrors().size()+">");
		
		
		if(StringUtils.isBlank(this.selectedGenre)&&StringUtils.isBlank(this.title)&&StringUtils.isBlank(this.author)&&StringUtils.isBlank(this.genre)){
			this.addActionError("At lest one criterion is needed.");
		//	this.addFieldError("genre", "I don't want to be here.");
//			if(StringUtils.isBlank(this.title))
//				this.addFieldError("title", "");
		}
//		System.out.println("actionError<"+this.getActionErrors().size()+">");
		super.validate();
	}

	
	@Override
	public String execute() throws Exception {
		// TODO Auto-generated method stub
		
		System.out.println("execute.");
		
		return this.doQuery();
		
//		return super.execute();
	}

	static class Book
	{
		private static final String BOOK_ID = "BOOK_ID";
		private static final String TITLE = "TITLE";
		private static final String AUTHOR = "AUTHOR";
		private static final String GENRE = "GENRE";
		private static final  String ISBN = "ISBN";
		private static final  String BLURB = "BLURB";
		
		public Book() {

		}
		private int bookId;
		private String title;
		private String author;
		private String genre;
		private String isbn;
		private String blurb;
		
		public int getBookId() {
			return bookId;
		}
		public void setBookId(int bookId) {
			this.bookId = bookId;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getAuthor() {
			return author;
		}
		public void setAuthor(String author) {
			this.author = author;
		}
		public String getGenre() {
			return genre;
		}
		public void setGenre(String genre) {
			this.genre = genre;
		}
		public String getIsbn() {
			return isbn;
		}
		public void setIsbn(String isbn) {
			this.isbn = isbn;
		}
		public String getBlurb() {
			return blurb;
		}
		public void setBlurb(String blurb) {
			this.blurb = blurb;
		}
	}
}
