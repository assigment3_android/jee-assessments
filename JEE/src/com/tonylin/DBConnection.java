package com.tonylin;
import java.sql.Connection;
import java.sql.SQLException;

import oracle.jdbc.pool.OracleDataSource;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;


public class DBConnection {
	private Connection conn = null;
	
	private static DBConnection instance;
	
	private DBConnection(){
		super();
	}
	
	
	public static Connection createConnection(){
		
		if (instance==null) instance = new DBConnection();
		
		try {
			if (instance.conn!=null&&(!instance.conn.isClosed())) return instance.conn;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		/*
		 * ==============================================================================================
		 * =========try to connect to the local mysql server if failed to connect to oracle============== 
		 * ============================================================================================== 
		 */
		try{// 
			MysqlDataSource ds = new MysqlDataSource();
			ds.setURL("jdbc:mysql://localhost/jee");
			ds.setPassword("j2ee");
			ds.setUser("jee");
			
			System.out.println("trying to connected jdbc:mysql://localhost/jee...");
			instance.conn = ds.getConnection();
			
			System.out.println("connected!");
			
		}catch (SQLException e){
			e.printStackTrace();
			System.out.println("fail to connect to Mysql");
		}
		
		
		/*
		 * ==============================================================================================
		 * ===============================try to connect to oracle server================================ 
		 * ============================================================================================== 
		 */
		if(instance.conn==null){
			try{			
				OracleDataSource ds = new OracleDataSource();
				// setup URL according to Oracle's specs
				ds.setURL("jdbc:oracle:thin:@docoraclere.unitec.ac.nz:1521:students");
				
				// set other data source properties
				ds.setPassword("TEST");
				ds.setUser("JCASEY");
				
				System.out.println("trying to conncect to jdbc:oracle:thin:@docoraclere.unitec.ac.nz:1521:students...");
				
				instance.conn = ds.getConnection();
				
				System.out.println("conncected!");
			}catch (SQLException e){
			//	e.printStackTrace();
				System.out.println("Fail to connect to jdbc:oracle:thin:@docoraclere.unitec.ac.nz:1521:students.");
				
			}
		}
		
		return instance.conn;
	}

	@Override
	protected void finalize() throws Throwable {
		if(this.conn!=null) this.conn.close();
		super.finalize();
	}
	
	
}
