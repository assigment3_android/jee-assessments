package com.tonylin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.dispatcher.ng.filter.StrutsPrepareAndExecuteFilter;

/**
 * Servlet Filter implementation class MyFilter
 */
@WebFilter("/MyFilter")
public class MyFilter extends StrutsPrepareAndExecuteFilter {

	@Override
    public void doFilter(ServletRequest req, ServletResponse res,FilterChain chain) throws IOException, ServletException {
        
		HttpServletRequest request = (HttpServletRequest) req;
        
		//不过滤的url,可以不断添加,如fck可用/fckeditor/editor/filemanager/connectors/fileupload,下面设置editor文件夹不过滤
		
     //   if (request.getRequestURI()!=null && request.getRequestURI().indexOf("editor")>0) {
		if(request.getRequestURI()!=null && request.getRequestURI().endsWith(".jsp")) {
            System.out.println("使用自定义的过滤器");
            
            HttpServletResponse reponse = (HttpServletResponse) res;
            reponse.setStatus(HttpServletResponse.SC_NOT_FOUND);
            
            
         //   chain.doFilter(req, reponse);
            
        }else{
            System.out.println("使用默认的过滤器  "+chain.toString());
            super.doFilter(req, res, chain);
            
        }
    }
}

