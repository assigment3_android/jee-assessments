<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ page import = "java.sql.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lab1</title>
</head>
 
<%
String param = request.getParameter("genre");
param = (param==null?"":param);
%>
<form action="lab1.jsp" method="post">
Genre:
<input name="genre" type="text" value="<%=param %>"/>
<input type="submit" />
</form>

<%
	Connection con = null;
	ResultSet rs = null;
	ResultSetMetaData rsmd = null;
	try{
		
	
		con = DriverManager.getConnection("jdbc:mysql://localhost/jee?user=jee&password=j2ee");
		
		if(param.equals("")){
			Statement sta = con.createStatement();
			rs = sta.executeQuery("SELECT * FROM Book");
		}else{
			PreparedStatement prepStat =  con.prepareStatement("SELECT * FROM Book WHERE genre=?");
			prepStat.setString(1, param);
			rs = prepStat.executeQuery();
			System.out.print(prepStat.toString());
		}
			rsmd = rs.getMetaData();
		//	System.out.print(rsmd.getColumnCount());
		
	}catch(SQLException e){
		e.printStackTrace();
	}
	
%>
<body>
	<table border="1">
	<tr align="center" bgcolor="#CCCCCC">
	<%
	int columnCount = rsmd.getColumnCount();
//	int columnCount = 6;
	for(int i=0; i<columnCount; i++){
		out.println("<td>"+rsmd.getColumnName(i+1)+"</td>");
	}
	%>
	</tr>
	<%
		while(rs!=null && rs.next()){
			out.println("<tr align='center'>");
			for(int i=0; i<columnCount; i++){
				out.println("<td>"+rs.getObject(i+1).toString()+"</td>");
			}
			out.println("</tr>");
		}
	%>
	</table>
</body>
</html>