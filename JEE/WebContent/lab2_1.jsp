<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="lab" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lab 2_1</title>
</head>
<body>

	<h2>Lab session 2_1 Fuzzy Search</h2>
	<lab:actionerror />
	<%
	System.out.println("query="+request.getParameter("query"));
	if(request.getParameter("query")!=null){
	%>
	<lab:fielderror fieldName="genre"/>
	<%} %>
	<hr/>
		
	<lab:form action="lab2_1.do?query=true" method="post">
	<lab:textfield name="title" key="label.title" size="20" />
	<lab:textfield name="author" key="label.author" size="20" />
	<lab:textfield name="genre" key="label.genre"  size="20" />
	
	<lab:checkbox name="fuzzySearch" value="false" key="label.isFuzzySearch" />
	
	<lab:submit name="execute" key="label.query" align="center" />
	
	
	</lab:form>
	
	<hr/>
	
	
	<table border="1">
	
	<lab:iterator value="bookList">
	<tr>
	<% 
		boolean isShowAllFields = true;
	
		if (isShowAllFields){
	%>
	<td><lab:property value="bookId" /></td>
	<td><lab:property value="title" /></td>
	<td><lab:property value="author" /></td>
	<td><lab:property value="genre" /></td>
	<td><lab:property value="isbn" /></td>
	
	<%
		}
	%>
	<td><lab:property value="blurb" /></td>
	</tr>
	</lab:iterator>
	
	</table>
	
</body>
</html>